#!/usr/bin/env bash

compile_wasm() {
    tinygo build -target=wasi -no-debug -opt=2 -gc=leaking -o dmeta.wasm module.go
    cp dmeta.wasm ./build/dmeta.wasm
    echo "dmeta wasm module compiled and stored in ./build/dmeta.wasm"
}


deploy_contract() {
    STORAGE=$(cat ./storage.tz)
    octez-client originate contract ex1 \
        transferring 0 from mainDev \
        running ./contract.tz \
        --init "$STORAGE" \
        --burn-cap 1.5 \
        --force
}

deploy_contract
