
# Quetzal - dmeta Example Contract

This includes the smart contract for the quetzals contract as well as the dmeta WASM module written in go.

To compile the dmeta wasm module:

```
tinygo build -target=wasi -no-debug -opt=2 -gc=leaking -o dmeta.wasm module.go

```


To compile the contract and deploy the contract:

```
# compile storage
ligo compile expression cameligo --init-file ./contract/contract.mligo contract_config > storage.tz

# compile contract
ligo compile contract contract/contract.mligo  > contract.tz 

# deploy contract
STORAGE=$(cat ./storage.tz)
    octez-client originate contract ex1 \
        transferring 0 from <address> \
        running ./contract.tz \
        --init "$STORAGE" \
        --burn-cap 1.5 \
        --force
```

On Tezos Mainnet the most recent version of this repo is deployed at:
https://better-call.dev/mainnet/KT1R1zAm8M2xEmiH12RiqtsbUFwCgYcE6wCN/operations

