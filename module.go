package main

import (
	"bytes"
	"embed"
	"encoding/hex"
	"errors"
	"fmt"
	"golang.org/x/image/font"
	"golang.org/x/image/font/opentype"
	"golang.org/x/image/math/fixed"
	"hash/fnv"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
)

//go:embed assets
var assets embed.FS

func main() {

	cmd := os.Getenv("CMD")
	myself := os.Getenv("CONTRACT")
	cmd = strings.Trim(cmd, " /")

	switch cmd {

	case "contractMetadata":
		showContractMetadata()
		return

	case "capabilities":
		printHeadersJSON()
		fmt.Println("[\"/contractMetadata\",\"/tokenMetadata/<tokenId>\"]")
		return
	}

	splits := strings.Split(cmd, "/")
	if len(splits) == 2 {
		if splits[0] == "tokenMetadata" {
			id, e := strconv.Atoi(splits[1])
			if e != nil {
				printError(errors.New("Invalid Token Id"))
			}
			traits := idToTraits(id)
			name, e := GetName(id)
			if e != nil {
				printError(errors.New("error reading name"))
				return
			}
			metadata := GetMetadata(id, name, myself)
			health, e := GetHealthData(id)
			if e != nil {
				printError(errors.New("error reading health"))
				return
			}
			for _, t := range traits {
				metadata.Attributes = append(metadata.Attributes, Attribute{
					Value: t.Value,
					Name:  t.Name,
				})
			}
			metadata.Attributes = append(metadata.Attributes, Attribute{
				Value: name,
				Name:  "Name",
			})
			h := health.HealthAdjusted()
			metadata.Attributes = append(metadata.Attributes, Attribute{
				Value: strconv.Itoa(h),
				Name:  "Health",
			})
			alive := "Alive"
			if h <= 0 {
				alive = "DEAD"
			}
			metadata.Attributes = append(metadata.Attributes, Attribute{
				Value: alive,
				Name:  "Status",
			})
			printHeadersJSON()
			fmt.Println(metadata.JSON())
			return
		}

		if splits[0] == "tokenImage" {
			id, e := strconv.Atoi(splits[1])
			if e != nil {
				printError(errors.New("Invalid Token Id"))
				return
			}
			traits := idToTraits(id)
			images := []string{"assets/reference.png"}
			for _, t := range traits {
				if t.Image != "none" {
					images = append(images, "assets/"+t.Image)
				}
			}
			result, e := layerImages(images)
			name, e := GetName(id)
			if e != nil {
				printError(errors.New("error reading name: " + e.Error()))
				return
			}
			DrawText(&result, name, 50)
			health, e := GetHealthData(id)
			if e != nil {
				printError(errors.New("error reading health data: " + e.Error()))
				return
			}
			var png []byte
			if health.HealthAdjusted() <= 0 {
				result2 := drawHealth(&result, health.HealthAdjusted())
				res3 := WithDead(*result2)
				if e != nil {
					printError(errors.New("error creating image"))
					return
				}
				png, _ = toPng(res3)
			} else {
				result2 := drawHealth(&result, health.HealthAdjusted())
				if e != nil {
					printError(errors.New("error creating image"))
					return
				}
				png, _ = toPng(*result2)
			}
			printHeadersPNG()
			hexWriter := hex.NewEncoder(os.Stdout)
			io.Copy(hexWriter, bytes.NewBuffer(png))
			fmt.Print("\n")
			return
		}
	}

	printError(errors.New("Path not implemented"))
}

type HealthData struct {
	Health     int
	LastUpdate time.Time
	Read       string
}

func NewHealthData(input string) (*HealthData, error) {
	hd := &HealthData{}
	hd.Read = input
	input = strings.Trim(input, "{}")
	pairs := strings.Split(input, ",")

	for _, pair := range pairs {
		indexOfColon := strings.Index(pair, ":")
		if indexOfColon == -1 {
			return nil, fmt.Errorf("invalid input format")
		}
		key := strings.Trim(pair[:indexOfColon], "\"")
		value := strings.Trim(pair[indexOfColon+1:], "\"")

		switch key {
		case "health":
			health, err := strconv.Atoi(value)
			if err != nil {
				return nil, fmt.Errorf("invalid health value: %v", err)
			}
			hd.Health = health
		case "last_update":
			t, err := time.Parse(time.RFC3339, value)
			if err != nil {
				return nil, fmt.Errorf("invalid time format: %v", err)
			}
			hd.LastUpdate = t
		default:
			// Optionally handle or log unknown keys
		}
	}

	return hd, nil
}

func (hd *HealthData) HealthAdjusted() int {
	now := time.Now()
	hoursPassed := now.Sub(hd.LastUpdate).Hours()
	x := hd.Health - int(hoursPassed)
	return x
}

func GetHealthData(id int) (*HealthData, error) {
	f, e := os.Open("./self/token_health/" + strconv.Itoa(id) + "/_data") // struct, virtual extra field
	if e != nil {
		return nil, e
	}
	b, _ := ioutil.ReadAll(f)
	return NewHealthData(string(b))
}

func GetName(id int) (string, error) {
	f, e := os.Open("./self/token_names/" + strconv.Itoa(id))
	if e != nil {
		return "", e
	}
	b, _ := ioutil.ReadAll(f)
	return strings.Trim(string(b), "\n"), nil
}

// WithDead overlays ./assets/dead.png on the original image with 95% opacity.
func WithDead(original image.Image) image.Image {
	// Load the overlay image
	overlayFile, err := assets.Open("assets/dead.png")
	if err != nil {
		panic(err) // Handle error more gracefully in real applications
	}
	defer overlayFile.Close()

	overlayImg, err := png.Decode(overlayFile)
	if err != nil {
		panic(err) // Handle error more gracefully in real applications
	}

	// Create a new image for the result
	bounds := original.Bounds()
	dst := image.NewRGBA(bounds)
	draw.Draw(dst, bounds, original, bounds.Min, draw.Src)

	// Calculate the overlay with 95% opacity
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			originalColor := original.At(x, y)
			overlayColor := overlayImg.At(x, y)

			r1, g1, b1, a1 := originalColor.RGBA()
			r2, g2, b2, a2 := overlayColor.RGBA()

			const opacity = 0.90
			r := (float64(r1)*(1-opacity) + float64(r2)*opacity) / 256
			g := (float64(g1)*(1-opacity) + float64(g2)*opacity) / 256
			b := (float64(b1)*(1-opacity) + float64(b2)*opacity) / 256
			a := (float64(a1)*(1-opacity) + float64(a2)*opacity) / 256

			dst.Set(x, y, color.RGBA{
				R: uint8(r),
				G: uint8(g),
				B: uint8(b),
				A: uint8(a),
			})
		}
	}

	return dst
}

func drawHealth(img *image.Image, health int) *image.Image {
	// Load the health indicator image
	healthImgFile, err := assets.Open("assets/health.png")
	if err != nil {
		panic(err) // Handle error appropriately in real code
	}
	defer healthImgFile.Close()

	healthImg, err := png.Decode(healthImgFile)
	if err != nil {
		panic(err) // Handle error appropriately in real code
	}

	// Calculate number of full and empty hearts
	fullHearts := health / 48
	if health%48 > 0 {
		fullHearts++
	}

	// Create a new image to avoid modifying the original
	dst := image.NewRGBA((*img).Bounds())
	draw.Draw(dst, dst.Bounds(), *img, image.Point{}, draw.Src)

	// Calculate position and draw hearts with padding
	padding := 25 // Padding from the edge
	heartWidth := healthImg.Bounds().Dx() / 2
	heartHeight := healthImg.Bounds().Dy()
	for i := 0; i < 5; i++ {
		var srcRect image.Rectangle
		if i < fullHearts {
			srcRect = image.Rect(0, 0, heartWidth, heartHeight) // Full heart
		} else {
			srcRect = image.Rect(heartWidth, 0, 2*heartWidth, heartHeight) // Empty heart
		}
		dstRect := image.Rect(
			dst.Bounds().Dx()-((5-i)*heartWidth)-padding, // Adjust for padding
			dst.Bounds().Dy()-heartHeight-padding,        // Adjust for padding
			dst.Bounds().Dx()-((4-i)*heartWidth)-padding, // Adjust for padding
			dst.Bounds().Dy()-padding,                    // Adjust for padding
		)
		draw.Draw(dst, dstRect, healthImg, srcRect.Min, draw.Over)
	}

	modifiedImg := image.Image(dst)
	return &modifiedImg
}

// DrawText draws the specified text onto the provided image at the upper left corner.
func DrawText(img *image.Image, text string, size int) error {
	if len(text) > 24 {
		text = text[:24]
	}
	// Load the font
	fontBytes, err := assets.ReadFile("assets/font.ttf")
	if err != nil {
		return fmt.Errorf("error reading font file: %w", err)
	}

	fnt, err := opentype.Parse(fontBytes)
	if err != nil {
		return fmt.Errorf("error parsing font: %w", err)
	}

	// Create a context for drawing
	face, err := opentype.NewFace(fnt, &opentype.FaceOptions{
		Size:    float64(size),
		DPI:     72,
		Hinting: font.HintingNone,
	})
	if err != nil {
		return fmt.Errorf("error creating font face: %w", err)
	}

	dr := *img
	b := dr.Bounds()
	dst := image.NewRGBA(b)
	draw.Draw(dst, b, dr, b.Min, draw.Src)

	// Set the starting position for the text
	point := fixed.Point26_6{
		X: fixed.Int26_6(60 << 6),          // Starting 10px from the left
		Y: fixed.Int26_6((60 + size) << 6), // Starting 30px from the top (to account for the font's height)
	}

	d := &font.Drawer{
		Dst:  dst,
		Src:  image.NewUniform(color.RGBA{R: 0, G: 0, B: 0, A: 255}), // Black color
		Face: face,
		Dot:  point,
	}
	d.DrawString(text)

	// Update the original image
	*img = dst

	return nil
}

func layerImages(imagePaths []string) (image.Image, error) {
	if len(imagePaths) == 0 {
		return nil, fmt.Errorf("no images provided")
	}

	// Open the first image to establish the base
	file, err := assets.Open(imagePaths[0])
	if err != nil {
		return nil, err
	}
	file.Close()

	baseImage, err := png.Decode(file)
	if err != nil {
		return nil, err
	}
	bounds := baseImage.Bounds()
	rgba := image.NewRGBA(bounds)

	// Draw the first image onto the base
	draw.Draw(rgba, bounds, baseImage, bounds.Min, draw.Src)

	// Layer the rest of the images
	for _, path := range imagePaths[1:] {
		f, err := assets.Open(path)
		if err != nil {
			return nil, err
		}

		img, err := png.Decode(f)
		f.Close()
		if err != nil {
			return nil, err
		}

		draw.Draw(rgba, bounds, img, bounds.Min, draw.Over)
	}

	return rgba, nil
}

func toPng(img image.Image) ([]byte, error) {
	var buf bytes.Buffer
	err := png.Encode(&buf, img)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

type Trait struct {
	Image string
	Name  string
	Value string
}

func idToTraits(id int) []Trait {
	beaks := []Trait{
		{
			Image: "beak/1.png",
			Value: "Pointy",
		},
		{
			Image: "beak/2.png",
			Value: "Orange",
		},
		{
			Image: "beak/3.png",
			Value: "Dark Orange",
		},
		{
			Image: "beak/4.png",
			Value: "Short Orange",
		},
		{
			Image: "beak/5.png",
			Value: "Appricot",
		},
		{
			Image: "beak/6.png",
			Value: "Small",
		},
		{
			Image: "beak/7.png",
			Value: "Elegant",
		},
		{
			Image: "beak/8.png",
			Value: "Pointy",
		},
		{
			Image: "beak/9.png",
			Value: "Curvy",
		},
		{
			Image: "beak/10.png",
			Value: "Plump",
		},
		{
			Image: "beak/11.png",
			Value: "Tiny",
		},
		{
			Image: "beak/12.png",
			Value: "Big",
		},
		{
			Image: "beak/13.png",
			Value: "Shiny",
		},
		{
			Image: "beak/13.png",
			Value: "Short",
		},
		{
			Image: "none",
			Value: "Dark",
		},
	}
	for i, _ := range beaks {
		beaks[i].Name = "Beak"
	}

	feathers := []Trait{
		{
			Image: "feathers/1.png",
			Value: "Big Red",
		},
		{
			Image: "feathers/2.png",
			Value: "Green",
		},
		{
			Image: "feathers/3.png",
			Value: "Long",
		},
		{
			Image: "feathers/4.png",
			Value: "Simple Green",
		},
		{
			Image: "feathers/5.png",
			Value: "Artistic",
		},
		{
			Image: "feathers/6.png",
			Value: "Straight",
		},
		{
			Image: "feathers/7.png",
			Value: "Red Ribbon",
		},
		{
			Image: "feathers/8.png",
			Value: "Flat",
		},
		{
			Image: "feathers/9.png",
			Value: "Pointy",
		},
		{
			Image: "feathers/10.png",
			Value: "Short",
		},
		{
			Image: "feathers/11.png",
			Value: "Straight Dark",
		},
		{
			Image: "feathers/12.png",
			Value: "Colorful",
		},
		{
			Image: "feathers/13.png",
			Value: "Minimalistic",
		},
		{
			Image: "feathers/14.png",
			Value: "Rounded",
		},
		{
			Image: "feathers/15.png",
			Value: "Shiny",
		},

		{
			Image: "feathers/16.png",
			Value: "Regular",
		},
		{
			Image: "none",
			Value: "Dark",
		},
	}
	for i, _ := range feathers {
		feathers[i].Name = "Feathers"
	}

	hair := []Trait{
		{
			Image: "hair/1.png",
			Value: "Pointed",
		},
		{
			Image: "hair/2.png",
			Value: "Wave",
		},
		{
			Image: "hair/3.png",
			Value: "Waves",
		},
		{
			Image: "hair/4.png",
			Value: "Almost Straight",
		},
		{
			Image: "hair/5.png",
			Value: "Baby",
		},
		{
			Image: "hair/6.png",
			Value: "Pointed Waves",
		},
		{
			Image: "hair/7.png",
			Value: "Short",
		},
		{
			Image: "hair/8.png",
			Value: "Stacked",
		},
		{
			Image: "hair/9.png",
			Value: "Stacked Long",
		},
		{
			Image: "hair/10.png",
			Value: "Long",
		},
		{
			Image: "hair/11.png",
			Value: "Simple",
		},
		{
			Image: "hair/12.png",
			Value: "Simple Wave",
		},
		{
			Image: "hair/13.png",
			Value: "Super Pointy",
		},
		{
			Image: "hair/14.png",
			Value: "curled",
		},
		{
			Image: "hair/15.png",
			Value: "Downwards",
		},

		{
			Image: "hair/16.png",
			Value: "Straight",
		},
		{
			Image: "none",
			Value: "Clean",
		},
	}
	for i, _ := range hair {
		hair[i].Name = "Hair"
	}

	wing := []Trait{
		{Image: "wing/1.png", Value: "Fluffy"},
		{Image: "wing/2.png", Value: "Simple"},
		{Image: "wing/3.png", Value: "Round"},
		{Image: "wing/4.png", Value: "Black Dot"},
		{Image: "wing/5.png", Value: "Unkempt"},
		{Image: "wing/6.png", Value: "Downy Soft"},
		{Image: "wing/7.png", Value: "Noisy"},
		{Image: "wing/8.png", Value: "Silky Strands"},
		{Image: "wing/9.png", Value: "Artistic"},
		{Image: "wing/10.png", Value: "Shield"},
		{Image: "wing/11.png", Value: "Rich colors"},
		{Image: "wing/12.png", Value: "Hidden Eye"},
		{Image: "wing/13.png", Value: "Straight"},
		{Image: "wing/14.png", Value: "Broad"},
		{Image: "wing/15.png", Value: "Straight Edges"},
		{Image: "wing/16.png", Value: "Elaborate"},
		{Image: "wing/17.png", Value: "Pearls"},
		{Image: "wing/18.png", Value: "Fluffy"},
		{Image: "wing/19.png", Value: "Amazing"},
		{Image: "wing/20.png", Value: "Scarred"},
		{Image: "wing/21.png", Value: "Sparkles"},
		{Image: "wing/22.png", Value: "Matte"},
		{Image: "wing/23.png", Value: "Super Shiny"},
		{Image: "wing/24.png", Value: "Simple"},
		{Image: "wing/25.png", Value: "Thick"},
		{Image: "wing/26.png", Value: "Striped"},
		{Image: "wing/27.png", Value: "Creative"},
		{Image: "wing/28.png", Value: "Decorated"},
		{Image: "wing/29.png", Value: "Layered"},
		{Image: "wing/30.png", Value: "Black Ribbon"},
		{Image: "wing/31.png", Value: "Pointy Red"},
		{Image: "wing/32.png", Value: "Drop"},
		{Image: "wing/33.png", Value: "Fancy"},
		{Image: "wing/34.png", Value: "Layered Thick"},
		{Image: "wing/35.png", Value: "Elegant"},
		{Image: "wing/36.png", Value: "Modern Art"},
		{Image: "wing/37.png", Value: "Sparkling Red"},
		{Image: "wing/38.png", Value: "Red Dot"},
		{Image: "wing/39.png", Value: "Jewelry"},
		{Image: "wing/40.png", Value: "Spots"},
		{Image: "wing/43.png", Value: "White Spots"},
		{Image: "wing/44.png", Value: "Super Straight"},
		{Image: "wing/45.png", Value: "Big Red"},
		{Image: "wing/46.png", Value: "Sunshine"},
		{Image: "wing/47.png", Value: "Flat"},
		{Image: "wing/48.png", Value: "Super Soft"},
		{Image: "wing/49.png", Value: "Simplistic"},
		{Image: "wing/50.png", Value: "Curved Elegance"},
		{Image: "wing/51.png", Value: "Dirty Green"},
		{Image: "wing/52.png", Value: "Gradient"},
		{Image: "wing/53.png", Value: "Red Eye"},
		{Image: "wing/54.png", Value: "Cirles"},
		{Image: "wing/55.png", Value: "Chaotic"},
		{Image: "wing/56.png", Value: "Dots"},
		{Image: "wing/57.png", Value: "Energy Burst"},
		{Image: "wing/58.png", Value: "Patchy"},
		{Image: "wing/59.png", Value: "Classic"},
		{Image: "none", Value: "Marbled"},
	}
	for i, _ := range wing {
		wing[i].Name = "Wing"
	}

	foods := []Trait{
		{Image: "none", Value: "Seeds"},
		{Image: "none", Value: "Fruits"},
		{Image: "none", Value: "Insects"},
		{Image: "none", Value: "Nectar"},
	}
	for i, _ := range foods {
		foods[i].Name = "Favourite Food"
	}

	hobbies := []Trait{
		{Image: "none", Value: "Singing"},
		{Image: "none", Value: "Dancing"},
		{Image: "none", Value: "Mimicking"},
		{Image: "none", Value: "Sleeping"},
		{Image: "none", Value: "Collecting Shiny Objects"},
	}

	for i, _ := range hobbies {
		hobbies[i].Name = "Hobby"
	}

	allTraits := [][]Trait{beaks, feathers, hair, wing, foods, hobbies}
	selectedTraits := make([]Trait, len(allTraits))

	hash := hashID(id)
	for i, traits := range allTraits {
		index := hash % uint32(len(traits))
		selectedTraits[i] = traits[index]
	}

	return selectedTraits

}

func hashID(id int) uint32 {
	h := fnv.New32a()
	h.Write([]byte(fmt.Sprintf("%d", id)))
	return h.Sum32()
}

func printError(e error) {
	fmt.Println("Content-Type: text/plain")
	fmt.Println("Encoding: none")
	fmt.Println("Compression: none")
	fmt.Println("Status-Code: 500")
	fmt.Println("Body:")
	fmt.Println(e.Error())
}

func printHeadersPNG() {
	fmt.Println("Content-Type: image/png")
	fmt.Println("Encoding: hex")
	fmt.Println("Compression: none")
	fmt.Println("Body:")
}

func printHeadersJSON() {
	fmt.Println("Content-Type: application/json")
	fmt.Println("Encoding: none")
	fmt.Println("Compression: none")
	fmt.Println("Body:")
}

func printHeadersTEXT() {
	fmt.Println("Content-Type: application/text")
	fmt.Println("Encoding: none")
	fmt.Println("Compression: none")
	fmt.Println("Body:")
}

func showContractMetadata() {
	printHeadersJSON()
	data := `{
		"name":        "Quetzals",
		"author":      "Johann Tanzer johann@tezoscommons.org",
		"website":     "https://quetzals.xyz",
		"description": "Quetzals is the first pfp collection using dynamic metadata on Tezos."
	}`
	fmt.Println(data)
}

func clean(input []byte) []byte {
	var result []byte

	for _, b := range input {
		if b >= 32 && b <= 126 {
			result = append(result, b)
		}
	}

	return result
}

func GetMetadata(tokenId int, name string, myself string) Collectible {
	return Collectible{
		ID:     strconv.Itoa(tokenId),
		Name:   name,
		Tags:   []string{"dmeta", "pfp", "generative", "animals", "bird", "quetzal"},
		Minter: "",
		Formats: []Format{
			{
				Uri:      "dmeta://" + myself + "tokenImage/" + strconv.Itoa(tokenId),
				MimeType: "image/png",
				Dimensions: Dimension{
					Unit:  "px",
					Value: "1024x1024",
				},
			},
		},
		Attributes:         []Attribute{},
		Creators:           []string{},
		Decimals:           "0",
		Royalties:          Royalties{},
		DisplayUri:         "dmeta://" + myself + "/tokenImage/" + strconv.Itoa(tokenId),
		Publishers:         []string{"https://quetzals.xyz"},
		ArtifactUri:        "dmeta://" + myself + "/tokenImage/" + strconv.Itoa(tokenId),
		Description:        "Quetzals are generative birds and a first demo of the dynamic metadata standard developed by Tezos Commons",
		ThumbnailUri:       "dmeta://" + myself + "/tokenImage/" + strconv.Itoa(tokenId),
		IsTransferable:     true,
		IsBooleanAmount:    true,
		ShouldPreferSymbol: false,
	}
}

type Collectible struct {
	ID                 string
	Name               string
	Tags               []string
	Minter             string
	Formats            []Format
	Creators           []string
	Decimals           string
	Royalties          Royalties
	Attributes         []Attribute
	DisplayUri         string
	Publishers         []string
	ArtifactUri        string
	Description        string
	ThumbnailUri       string
	IsTransferable     bool
	IsBooleanAmount    bool
	ShouldPreferSymbol bool
}

type Format struct {
	Uri        string
	MimeType   string
	Dimensions Dimension
}

type Dimension struct {
	Unit  string
	Value string
}

type Royalties struct {
	Shares   map[string]int
	Decimals string
}

type Attribute struct {
	Name  string
	Value string
}

func (c *Collectible) JSON() string {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf(`{"ID":"%s","Name":"%s","Tags":%s,"Minter":"%s","Formats":%s,"Creators":%s,"Decimals":"%s","Royalties":%s,"Attributes":%s,"DisplayUri":"%s","Publishers":%s,"ArtifactUri":"%s","Description":"%s","ThumbnailUri":"%s","IsTransferable":%t,"IsBooleanAmount":%t,"ShouldPreferSymbol":%t}`,
		c.ID, c.Name, arrayToString(c.Tags), c.Minter, formatsToString(c.Formats), arrayToString(c.Creators), c.Decimals, royaltiesToString(c.Royalties), attributesToString(c.Attributes), c.DisplayUri, arrayToString(c.Publishers), c.ArtifactUri, c.Description, c.ThumbnailUri, c.IsTransferable, c.IsBooleanAmount, c.ShouldPreferSymbol))
	return sb.String()
}

func arrayToString(a []string) string {
	var result []string
	for _, v := range a {
		result = append(result, fmt.Sprintf(`"%s"`, v))
	}
	return fmt.Sprintf("[%s]", strings.Join(result, ","))
}

func formatsToString(f []Format) string {
	var result []string
	for _, format := range f {
		result = append(result, fmt.Sprintf(`{"Uri":"%s","MimeType":"%s","Dimensions":{"Unit":"%s","Value":"%s"}}`, format.Uri, format.MimeType, format.Dimensions.Unit, format.Dimensions.Value))
	}
	return fmt.Sprintf("[%s]", strings.Join(result, ","))
}

func royaltiesToString(r Royalties) string {
	shares := "{"
	for k, v := range r.Shares {
		shares += fmt.Sprintf(`"%s":%d,`, k, v)
	}
	shares = strings.TrimRight(shares, ",")
	shares += "}"
	return fmt.Sprintf(`{"Shares":%s,"Decimals":"%s"}`, shares, r.Decimals)
}

func attributesToString(a []Attribute) string {
	var result []string
	for _, attr := range a {
		result = append(result, fmt.Sprintf(`{"Name":"%s","Value":"%s"}`, attr.Name, attr.Value))
	}
	return fmt.Sprintf("[%s]", strings.Join(result, ","))
}
